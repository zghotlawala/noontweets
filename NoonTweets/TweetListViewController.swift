//
//  ViewController.swift
//  NoonTweets
//
//  Created by mac on 11/18/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import UIKit
import MBProgressHUD



class TweetListViewController: UITableViewController,TwitterTweetDelegate, UISearchBarDelegate {

    var twitterAPI: TwitterApiHandler = TwitterApiHandler()
    var arrTweets = [TweetDetails]()
    var queryString = "#Dubai";
    var timer : Timer!
    let key_ReloadTimer = "RELOAD-TIMER"
    @IBOutlet weak var searchBar: UISearchBar!
    
//    MARK: View Handlers
    override func didReceiveMemoryWarning() {
        NSLog("HELP ME - TweetListViewController");
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseTableView();
    }
    
    func initialiseTableView(){
        self.tableView.register(TweetTableViewCell.classForCoder(), forCellReuseIdentifier: "TweetTableViewCell");
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.allowsSelection = false;
        self.tableView.tableHeaderView = searchBar;
        self.definesPresentationContext = true
        self.tableView.keyboardDismissMode = .onDrag
        self.refreshControl = UIRefreshControl();
        self.refreshControl?.addTarget(self, action: #selector(TweetListViewController.refreshTable), for: .valueChanged);
        self.tableView.refreshControl = self.refreshControl
        
        searchBar.text = queryString
        relaodTweets(withLabel:queryString,  isRefresh:false, isLoadMore:false);
    }
    
//    MARK: Timer Handler
    func checkTimerSettings(){
        let seconds = UserDefaults.standard.float(forKey: self.key_ReloadTimer)
        if seconds != 0{
            startTimer(withSeconds: seconds );
        }else{
            UserDefaults.standard.set(MAXFLOAT, forKey: self.key_ReloadTimer)
            startTimer(withSeconds: MAXFLOAT);
        }
    }
    
    func startTimer(withSeconds: Float){
        timer?.invalidate();
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(withSeconds),
                                     target: self,
                                     selector: #selector(TweetListViewController.handleTimer),
                                     userInfo: nil,
                                     repeats: true)
        let mainLoop = RunLoop.main
        mainLoop.add(timer, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    func handleTimer(){
        relaodTweets(withLabel: nil, isRefresh:true, isLoadMore:false);
    }
    
    @IBAction func settingsBtnPressed(_ sender: Any) {
        timer?.invalidate();
        
        let alert = UIAlertController(title: "Automatic Reload Options", message: "", preferredStyle: UIAlertControllerStyle.actionSheet);
        
                let every2SecAutoReload = UIAlertAction(title: "Every 2 second reload", style: .default){ alert in
            UserDefaults.standard.set(2.0, forKey: self.key_ReloadTimer)
            self.startTimer(withSeconds: 2.0);
        }
        let every5SecAutoReload = UIAlertAction(title: "Every 5 second reload", style: .default){ alert in
            UserDefaults.standard.set(5.0, forKey: self.key_ReloadTimer)
            self.startTimer(withSeconds: 5.0);
        }
        let every30SecAutoReload = UIAlertAction(title: "Every 30 second reload", style: .default){ alert in
            UserDefaults.standard.set(30.0, forKey: self.key_ReloadTimer)
            self.startTimer(withSeconds: 30.0);
        }
        
        let every60SecAutoReload = UIAlertAction(title: "Every 1 minute reload", style: .default){ alert in
            UserDefaults.standard.set(60.0, forKey: self.key_ReloadTimer)
            self.startTimer(withSeconds: 60.0);
        }
        let disableAutoReload = UIAlertAction(title: "No Refresh", style: .default){ alert in
            self.timer?.invalidate();
            UserDefaults.standard.set(MAXFLOAT, forKey: self.key_ReloadTimer)
        }

        let later = UIAlertAction(title: "Later", style: .destructive){ alert in
            
            let seconds = UserDefaults.standard.float(forKey: self.key_ReloadTimer)
            if seconds != 0{
                self.startTimer(withSeconds: seconds);
            }else{
                UserDefaults.standard.set(MAXFLOAT, forKey:self.key_ReloadTimer)
                self.startTimer(withSeconds: MAXFLOAT);
            }
        }
        
        alert.addAction(disableAutoReload);
        alert.addAction(every2SecAutoReload);
        alert.addAction(every5SecAutoReload);
        alert.addAction(every30SecAutoReload);
        alert.addAction(every60SecAutoReload);
        alert.addAction(later);
        
        self.present(alert, animated: true, completion: nil);
    }
    
//    MARK: Data Handler
    
    func refreshTable(){
        requestFeeds(true, isLoadMore: false);
    }
    
    func relaodTweets(withLabel:String?, isRefresh:Bool, isLoadMore:Bool){
        
        if self.refreshControl?.isRefreshing == false{
            var label = "Requesting";
            if let _ = withLabel{
                label += " "
                label += withLabel!;
            }
            label += " Tweets";
            
            DispatchQueue.main.async {
                HUD?.hide(animated: false);
                HUD = MBProgressHUD.showAdded(to: self.view.window!, animated: true);
                HUD.mode = .indeterminate
                HUD.detailsLabel.text = label;
            }
         
            requestFeeds(isRefresh, isLoadMore: isLoadMore);
        }
    }
    
    func requestFeeds(_ isRefresh:Bool, isLoadMore:Bool){
        var searchStrURL = ""
        
        if isRefresh{
            if let refreshURL = twitterAPI.searchMeta?.refreshURL{
                searchStrURL = refreshURL;
                
                if let count = twitterAPI.searchMeta?.count{
                    searchStrURL = searchStrURL + "&count="+String(count)
                }else{
                    searchStrURL = "";
                    
                }
            }
        }else if isLoadMore{
            if let nextResultURL = twitterAPI.searchMeta?.nextResultURL{
                searchStrURL = nextResultURL
                if let count = twitterAPI.searchMeta?.count{
                    searchStrURL = searchStrURL + "&count="+String(count)
                }else{
                    searchStrURL = "";
                    if self.refreshControl?.isRefreshing == true{
                        self.refreshControl?.endRefreshing();
                    }
                    if HUD?.isHidden == false{
                        HUD.detailsLabel.text = "No more records available";
                        HUD.hide(animated: true, afterDelay: 2.0)
                    }
                    return;
                }
            }else{
                if self.refreshControl?.isRefreshing == true{
                    self.refreshControl?.endRefreshing();
                }
                return;
            }
        }
        
        if searchStrURL.isEmpty{
            let customAllowedSet =  NSCharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}").inverted
            let escapedString = "?q=" + queryString.addingPercentEncoding( withAllowedCharacters: customAllowedSet)!
            searchStrURL = escapedString;
        }
        
        twitterAPI.delegate = self
        let urlString =  searchStrURL;
        twitterAPI.getResponseForRequest(url: urlString, isRefresh : isRefresh, isLoadMore: isLoadMore);
    }
    
    func loadMore(){
        self.relaodTweets(withLabel: "more", isRefresh: false, isLoadMore: true)
    }
    
//    MARK: Twitter Api Delegates
    func finishedDownloadingAll(arrTweetObj:[TweetDetails]?, makeReset:Bool){
        
        DispatchQueue.main.async {
            if self.refreshControl?.isRefreshing == true{
                self.refreshControl?.endRefreshing();
            }
            
            if makeReset{
                self.arrTweets.removeAll();
                if let _ = arrTweetObj{
                    if HUD?.isHidden == false{
                        HUD.hide(animated: true, afterDelay: 0);
                    }
                    self.arrTweets = self.arrTweets + arrTweetObj!
                }else{
                    HUD?.detailsLabel.text = "Error in requesting feed"
                    HUD?.mode = .text;
                    HUD?.hide(animated: true, afterDelay: 2.0)
                }
            }else{
                if HUD?.isHidden == false{
                    HUD.hide(animated: true, afterDelay: 0);
                }
                if arrTweetObj != nil{
                    self.arrTweets = self.arrTweets + arrTweetObj!
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func finishedDownloading(tweet:TweetDetails?, makeReset:Bool){
        
        DispatchQueue.main.async {
            if self.refreshControl?.isRefreshing == true{
                self.refreshControl?.endRefreshing();
            }
            
            if makeReset{
                self.arrTweets.removeAll();
                if let _ = tweet{
                    if HUD?.isHidden == false{
                        HUD.hide(animated: true, afterDelay: 0);
                    }
                    self.arrTweets.append(tweet!);
                }else{
                    HUD?.detailsLabel.text = "Error in requesting feed"
                    HUD?.mode = .text;
                    HUD?.hide(animated: true, afterDelay: 2.0)
                }
            }else{
                if HUD?.isHidden == false{
                    HUD.hide(animated: true, afterDelay: 0);
                }
                
                if tweet != nil{
                    self.arrTweets.append(tweet!)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func finishedDownloadingWithError(error: Error?){
    
        DispatchQueue.main.async {
            if self.refreshControl?.isRefreshing == true{
                self.refreshControl?.endRefreshing();
            }
            HUD?.label.text = "Error in requesting feed "
            if let wrapper = error{
                HUD?.detailsLabel.text =  wrapper.localizedDescription;
                
            }
            HUD?.mode = .text;
            HUD?.hide(animated: true, afterDelay: 2.0)
        }
    
    }
    
//    MARK: Table Handlers, delegates and datasource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTweets.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TweetTableViewCell", for: indexPath)
            as! TweetTableViewCell
        let tweetDetails = arrTweets[indexPath.row];
        
        cell.tweetLabel.tag = indexPath.row;
        self.configureCellWithItem(cell, tweetDetails: tweetDetails)
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let _ = twitterAPI.searchMeta?.nextResultURL{
            return 50
        }
        return 0;
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let _ = twitterAPI.searchMeta?.nextResultURL{
            let footerView = UIView(frame: CGRect(x:0,y:0,width:tableView.frame.width, height: 50));
            let reloadMore = UIButton(forAutoLayout:());
            footerView.addSubview(reloadMore);
            reloadMore.autoSetDimensions(to: CGSize(width: 150, height: 40));
            reloadMore.autoCenterInSuperview();
            reloadMore.addTarget(self, action: #selector(TweetListViewController.loadMore), for: .touchUpInside);
            reloadMore.titleLabel?.text = ""
            reloadMore.layer.borderColor = UIColor.darkGray.cgColor;
            reloadMore.setTitle("More Records?", for: .normal)
            reloadMore.setTitleColor(.black, for: .normal);
            reloadMore.setTitleColor(.gray, for: .normal);
            return footerView;
        }
        
        return nil;
    }
    
    func configureCellWithItem(_ cell:TweetTableViewCell, tweetDetails: TweetDetails) {
        if tweetDetails.profileURL != nil{
            cell.userPhotoImageView?.image = UIImage(data: tweetDetails.profileURL! as Data)
        }
        cell.userNameLabel?.text = tweetDetails.user
        cell.tweetLabel?.attributedText = tweetDetails.attributedTweet;
        cell.userPhotoImageView.clipsToBounds = true;
        cell.userPhotoImageView.layer.cornerRadius = 5.0;
        cell.tap.addTarget(self, action: #selector(TweetListViewController.tagPressed(_:)))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty == false{
            queryString = searchBar.text!;
            relaodTweets(withLabel:searchBar.text, isRefresh: false,isLoadMore: false);
        }
        searchBar.resignFirstResponder();
    }
    
    func tagPressed(_ tapRecongnizer: UITapGestureRecognizer){
        
        let tweetLbl = tapRecongnizer.view as! UILabel;
        let location = tapRecongnizer.location(in: tweetLbl);
        let tweetDetails = arrTweets[tweetLbl.tag];
        let textStorage = NSTextStorage(attributedString: tweetDetails.attributedTweet!);
        let layoutManager : NSLayoutManager = NSLayoutManager();
        textStorage.addLayoutManager(layoutManager);
        let textContainer : NSTextContainer = NSTextContainer(size: tweetLbl.bounds.size);
        layoutManager.addTextContainer(textContainer);
        textContainer.maximumNumberOfLines = tweetLbl.numberOfLines
        textContainer.lineBreakMode = tweetLbl.lineBreakMode;
        let characterIndex = layoutManager.characterIndex(for: location, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil);
        
        if characterIndex < textStorage.length{
            var range : NSRange? = NSMakeRange(0, 1)
            let attributes : Dictionary = textStorage.attributes(at:characterIndex, effectiveRange: &range!) ;
            
            if let _ = attributes["zHashTag"]{
                let value = tweetDetails.attributedTweet.string.substring(with: tweetDetails.attributedTweet.string.range(from: range!)!);
                let lightGrayColor = UIColor.lightGray;
                tweetDetails.attributedTweet.setAttributes([NSForegroundColorAttributeName : lightGrayColor], range: range!)
                tweetLbl.attributedText = tweetDetails.attributedTweet;
                queryString = value;
                searchBar.text = "#"+value;
                relaodTweets(withLabel:value, isRefresh: false,isLoadMore: false);
            }
            else if let _ = attributes["zUserName"]{
                let value = tweetDetails.attributedTweet.string.substring(with: tweetDetails.attributedTweet.string.range(from: range!)!);
                let lightGrayColor = UIColor.lightGray;
                tweetDetails.attributedTweet.setAttributes([NSForegroundColorAttributeName : lightGrayColor], range: range!)
                tweetLbl.attributedText = tweetDetails.attributedTweet;
                searchBar.text = "@"+value;
                queryString = value;
                relaodTweets(withLabel:value, isRefresh: false,isLoadMore: false);
            }
        }
    }
    
    
}

extension String {
    
    public func convertRangeToNSRange(r:Range<String.Index>) -> NSRange {
        
        let a   =   substring(to:r.lowerBound)
        let b   =   substring(with: r)
        
        return  NSRange(location: a.utf16.count, length: b.utf16.count)
    }
    
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = String.Index(from16, within: self),
            let to = String.Index(to16, within: self)
            else { return nil }
        return from ..< to
    }
}
