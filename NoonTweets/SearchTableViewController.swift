//
//  SearchTableViewController.swift
//  NoonTweets
//
//  Created by mac on 11/19/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol SearchTableViewControllerDelegate: class {
    func searchTableViewController(_ controller: SearchTableViewController, didSelectHandler handler:JSON)
}
class SearchTableViewController: UITableViewController, UISearchResultsUpdating,TwitterTweetDelegate {
     var twitterAPI: TwitterApiHandler = TwitterApiHandler()
    var arrTweets = [TweetDetails]()
    
    weak var delegate: SearchTableViewControllerDelegate?
    var arrResults :NSMutableArray!;
    var queryString = "q=%40Dubai";
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(TweetTableViewCell.classForCoder(), forCellReuseIdentifier: "TweetTableViewCell");
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
        self.tableView.keyboardDismissMode = .onDrag;
        
    }
    
    //    MARK: Search Results Handler
    func updateSearchResults(for searchController: UISearchController) {
        let whitespaceCharacterSet = CharacterSet.whitespaces
        let strippedString = searchController.searchBar.text!.trimmingCharacters(in: whitespaceCharacterSet)
        if strippedString.isEmpty{
            return;
        }
        let searchStrURL = "https://api.twitter.com/1.1/search/tweets.json?"+strippedString;
        twitterAPI.delegate = self;
        twitterAPI.getResponseForRequest(url: searchStrURL);
    }
    func finishedDownloading(tweets tweet: TweetDetails?, makeReset reset:Bool) {
        
        DispatchQueue.main.async {
           
            
            if reset{
                if HUD?.isHidden == false{
                    HUD.hide(animated: true, afterDelay: 0);
                }
                self.arrTweets.removeAll();
                if let _ = tweet{
                    self.arrTweets.append(tweet!);
                }else{
                    HUD?.detailsLabel.text = "Error in requesting feed"
                    HUD?.mode = .text;
                    HUD?.hide(animated: true, afterDelay: 2.0)
                }
            }else{
                self.arrTweets.append(tweet!)
                
            }
            self.tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTweets.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TweetTableViewCell", for: indexPath)
            as! TweetTableViewCell
        let tweetDetails = arrTweets[indexPath.row];
        cell.tweetLabel.tag = indexPath.row;
        self.configureCellWithItem(cell, tweetDetails: tweetDetails)
        
        
        return cell
    }
    func configureCellWithItem(_ cell:TweetTableViewCell, tweetDetails: TweetDetails) {
        if tweetDetails.profileURL != nil{
            cell.userPhotoImageView?.image = UIImage(data: tweetDetails.profileURL! as Data)
        }
        cell.userNameLabel?.text = tweetDetails.user
        cell.tweetLabel?.attributedText = tweetDetails.attributedTweet;
        cell.userPhotoImageView.clipsToBounds = true;
        cell.userPhotoImageView.layer.cornerRadius = cell.userPhotoImageView.frame.size.width * 0.075;
        cell.tap.addTarget(self, action: #selector(TweetListViewController.tagPressed(_:)))
        
        
    }
    
    func tagPressed(_ tapRecongnizer: UITapGestureRecognizer){
        
        let tweetLbl = tapRecongnizer.view as! UILabel;
        let location = tapRecongnizer.location(in: tweetLbl);
        let tweetDetails = arrTweets[tweetLbl.tag];
        let textStorage = NSTextStorage(attributedString: tweetDetails.attributedTweet!);
        let layoutManager : NSLayoutManager = NSLayoutManager();
        textStorage.addLayoutManager(layoutManager);
        let textContainer : NSTextContainer = NSTextContainer(size: tweetLbl.bounds.size);
        layoutManager.addTextContainer(textContainer);
        textContainer.maximumNumberOfLines = tweetLbl.numberOfLines
        textContainer.lineBreakMode = tweetLbl.lineBreakMode;
        let characterIndex = layoutManager.characterIndex(for: location, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil);
        if characterIndex < textStorage.length{
            var range : NSRange? = NSMakeRange(0, 1)
            let attributes : Dictionary = textStorage.attributes(at:characterIndex, effectiveRange: &range!) ;
            
            if let _ = attributes["zHashTag"]{
                let value = tweetDetails.attributedTweet.string.substring(with: tweetDetails.attributedTweet.string.range(from: range!)!);
                NSLog("zHashTag \(value)");
                let lightGrayColor = UIColor.lightGray;
                tweetDetails.attributedTweet.setAttributes([NSForegroundColorAttributeName : lightGrayColor], range: range!)
                tweetLbl.attributedText = tweetDetails.attributedTweet;
                queryString = "q=%23" + value;
                relaodTweets(withLabel:value,  isUser:false);
            }else if let _ = attributes["zUserName"]{
                let value = tweetDetails.attributedTweet.string.substring(with: tweetDetails.attributedTweet.string.range(from: range!)!);
                let lightGrayColor = UIColor.lightGray;
                tweetDetails.attributedTweet.setAttributes([NSForegroundColorAttributeName : lightGrayColor], range: range!)
                tweetLbl.attributedText = tweetDetails.attributedTweet;
                NSLog("username \(value)");
                queryString = "q=%40" + value;
                relaodTweets(withLabel:value,  isUser:true);
            }
        }
    }

    

    
    
}
