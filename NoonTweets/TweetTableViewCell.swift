//
//  TweetTableViewCell.swift
//  NoonTweets
//
//  Created by mac on 11/18/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import Foundation
let FontLarge = UIFont(name: "Helvetica Neue", size: 14);
let FontMedium = UIFont(name: "Helvetica Neue", size: 14);
let FontSmall = UIFont(name: "Helvetica Neue", size: 10);
let ColorMainText:UIColor = .darkText;
let ColorBodyText:UIColor = .lightGray;
class TweetTableViewCell: UITableViewCell {
    
    var userPhotoImageView: UIImageView!
    
    var userNameLabel: UILabel!
    var tweetLabel: UILabel!
    var tap: UITapGestureRecognizer!;
    
//    MARK: Initialiser
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        userPhotoImageView = UIImageView(forAutoLayout: ());
        userPhotoImageView.backgroundColor = .clear;
        self.contentView.addSubview(userPhotoImageView);
        let contentSize = self.contentView.frame.size;
        userPhotoImageView.autoSetDimension(.width, toSize: contentSize.width*0.2); // 20% of width;
        userPhotoImageView.autoMatch(.width, to: .height, of: userPhotoImageView);
        userPhotoImageView.autoPinEdge(toSuperviewEdge: .left, withInset: contentSize.width*0.025);
        userPhotoImageView.autoAlignAxis(toSuperviewAxis: .horizontal);
        
        let rightHandSideView = UIView(forAutoLayout: ());
        rightHandSideView.backgroundColor = .clear;
        self.contentView.addSubview(rightHandSideView);
        rightHandSideView.autoPinEdge(.left, to: .right, of: userPhotoImageView, withOffset: contentSize.width*0.015);
        rightHandSideView.autoPinEdge(toSuperviewEdge: .top, withInset: contentSize.width*0.025);
        rightHandSideView.autoPinEdge(toSuperviewEdge: .right, withInset: contentSize.width*0.025);
        rightHandSideView.autoPinEdge(toSuperviewEdge: .bottom, withInset: contentSize.width*0.025);
        
        userPhotoImageView.autoMatch(.height, to: .height, of: rightHandSideView, withOffset: 0, relation: .lessThanOrEqual);
        
        userNameLabel = UILabel(forAutoLayout: ());
        userNameLabel.backgroundColor = .clear;
        userNameLabel.textColor = ColorMainText;
        userNameLabel.textAlignment = .natural;
        userNameLabel.font = FontLarge;
        rightHandSideView.addSubview(userNameLabel);
        userNameLabel.numberOfLines = 0;
        userNameLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsetsMake(0, 0, 0, 0), excludingEdge: .bottom);
        
        tweetLabel = UILabel(forAutoLayout: ());
        tweetLabel.backgroundColor = .clear;
        tweetLabel.isUserInteractionEnabled = true;
        tweetLabel.numberOfLines = 0;
        tweetLabel.contentMode = .topLeft;
        userNameLabel.textAlignment = .justified;
        tweetLabel.textColor = ColorBodyText;
        tweetLabel.font = FontMedium;
        rightHandSideView.addSubview(tweetLabel);
        tweetLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 0, relation: .greaterThanOrEqual);
        tweetLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 0);
        tweetLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 0);
        tweetLabel.autoPinEdge(.top, to: .bottom, of: userNameLabel, withOffset: 2);
        
        tap = UITapGestureRecognizer() // Added to capture end user taps
        tap.numberOfTapsRequired = 1;
        
        tweetLabel.addGestureRecognizer(tap);
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
