//
//  TweetMetaData.swift
//  NoonTweets
//
//  Created by mac on 11/19/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import Foundation
import SwiftyJSON
class TweetMetaData {
    var searchMeta : JSON!
    var refreshURL : String!
    var nextResultURL : String!
    var count : Int!;
    var maxId : String!;
    var sinceId : String!;
    var query : String!;
    
//    MARK: Initialiser
    init(metaData : JSON) {
        self.searchMeta = metaData;
        
        if self.searchMeta["refresh_url"].exists(){
            refreshURL = self.searchMeta["refresh_url"].string!
        }
        if self.searchMeta["next_results"].exists(){
            nextResultURL = self.searchMeta["next_results"].string!
        
        }
        if self.searchMeta["count"].exists(){
            count = self.searchMeta["count"].int!
        
        }
        if self.searchMeta["since_id_str"].exists(){
            sinceId = self.searchMeta["since_id_str"].string!
        
        }
        if self.searchMeta["max_id_str"].exists(){
            maxId = self.searchMeta["max_id_str"].string!
        
        }
        if self.searchMeta["query"].exists(){
            query = self.searchMeta["query"].string!
            
        
        }
    }
}
