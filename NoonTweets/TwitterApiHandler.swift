//
//  TwitterApiHandler.swift
//  NoonTweets
//
//  Created by mac on 11/18/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol TwitterTweetDelegate{
    func finishedDownloading(tweet:TweetDetails?, makeReset:Bool)
    func finishedDownloadingAll(arrTweetObj:[TweetDetails]?, makeReset:Bool)
    func finishedDownloadingWithError(error: Error?);
}

public class TwitterApiHandler:NSObject {
    
    var delegate:TwitterTweetDelegate?
    
    let consumerKey = "P9YaR4XEvmuEWQjVdUrnw0KY7"
    let consumerSecret = "107R3Zk5AnGNgeJgB7wiCa1z6NkVsRBP5WTIz76ml8d97sjOPS"
    let host = "api.twitter.com"
    let searchAPI = "https://api.twitter.com/1.1/search/tweets.json"
    var searchMeta : TweetMetaData!;
    var isRefresh :Bool!;
    var isLoadMore : Bool!
    
    // MARK: Bearer Token
    func getBearerToken(completion:@escaping (_ bearerToken: String) ->Void) {
        
        let components = NSURLComponents()
        components.scheme = "https";
        components.host = self.host
        components.path = "/oauth2/token";
        
        let url = components.url;
        
        let request = NSMutableURLRequest(url:url!)
        
        request.httpMethod = "POST"
        request.addValue("Basic " + getBase64EncodeString(), forHTTPHeaderField: "Authorization")
        request.addValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        let grantType =  "grant_type=client_credentials"
        
        request.httpBody = grantType.data(using: String.Encoding.utf8, allowLossyConversion: true)
        
        
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            (data:Data?, response:URLResponse?, error: Error?) -> Void in
            do {
                if error == nil{
                if let results: NSDictionary = try JSONSerialization .jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments  ) as? NSDictionary {
                    if let token = results["access_token"] as? String {
                        completion(token)
                    } else {
                        print(results["errors"] ?? "unknown error")
                    }
                }
                }else{
                    self.delegate?.finishedDownloadingWithError(error: error);
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }).resume();
    }
    
    // MARK: base64Encode String
    func getBase64EncodeString() -> String {
        
        let consumerKeyRFC1738 = consumerKey.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        let consumerSecretRFC1738 = consumerSecret.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        
        let concatenateKeyAndSecret = consumerKeyRFC1738! + ":" + consumerSecretRFC1738!
        
        let secretAndKeyData = concatenateKeyAndSecret.data(using: String.Encoding.ascii, allowLossyConversion: true)
        
        let base64EncodeKeyAndSecret = secretAndKeyData?.base64EncodedString(options: []);
        
        return base64EncodeKeyAndSecret!
    }
    
    // MARK: Service Call
    func getResponseForRequest(url:String, isRefresh:Bool, isLoadMore:Bool) {
        self.isRefresh = isRefresh
        self.isLoadMore = isLoadMore;
        getBearerToken(completion: { (bearerToken) -> Void in
            let searchURL = self.searchAPI+url;
            let request = NSMutableURLRequest(url: NSURL(string: searchURL)! as URL)
            request.httpMethod = "GET"
            
            let token = "Bearer " + bearerToken
            
            request.addValue(token, forHTTPHeaderField: "Authorization")
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
                (data:Data?, response:URLResponse?, error: Error?) -> Void in
                if error == nil{
                self.processResult(data: data! as NSData, response: response!, error: error as NSError?)
                }else{
                    self.delegate?.finishedDownloadingWithError(error: error);
                }
            }).resume();
        })
        
    }
    
    // MARK: Process results
    func processResult(data: NSData, response:URLResponse, error: NSError?) {
        var makereset = true;
        let sendOneTime = true;  // BY SETTING THIS FALSE DATA WILL DISPLAY ONE BY ONE
        let json = JSON(data: data as Data)
        
        if  json["search_metadata"].exists(){
            searchMeta = TweetMetaData(metaData: json["search_metadata"])
        }
        if let tweets = json["statuses"].array{
            if tweets.count > 0 {
                if sendOneTime{
                    var arrTweets = [TweetDetails]();
                    for tweet in tweets{
                        let tweetObj = TweetDetails(tweet)
                        arrTweets.append(tweetObj);
                    }
                    self.delegate?.finishedDownloadingAll(arrTweetObj: arrTweets, makeReset: isLoadMore! ? false : makereset)
                }else{
                    for tweet in tweets{
                        let tweetObj = TweetDetails(tweet)
                        if isLoadMore == true{
                            self.delegate?.finishedDownloading(tweet: tweetObj, makeReset: false)
                        }else{
                            self.delegate?.finishedDownloading(tweet: tweetObj, makeReset: makereset)
                        }
                        makereset = false;
                    }
                }
            }else if isRefresh == true{
                self.delegate?.finishedDownloading(tweet: nil, makeReset: false);
            }else{
                self.delegate?.finishedDownloading(tweet: nil, makeReset: makereset)
            }
        }else{
                self.delegate?.finishedDownloading(tweet: nil , makeReset: makereset)
        }
        
        
    }
    
}
