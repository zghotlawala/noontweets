//
//  TweetsDetails.swift
//  NoonTweets
//
//  Created by mac on 11/18/16.
//  Copyright © 2016 Zahur Ghotlawala. All rights reserved.
//

import Foundation
import SwiftyJSON


class TweetDetails {
    var user: String = ""
    var tweet: String = ""
    var profileURL: NSData?
    var retweeted : Bool = false
    var retweetCount : Int = -1;
    var arrHashtags : [String]!;
    var dicTweet : JSON!
    var attributedTweet : NSMutableAttributedString!;

//    MARK: Initialiser
    init (_ dicTweet:JSON) {
        self.dicTweet = dicTweet;
        tweet = dicTweet["text"].string!;
        if dicTweet["entities"]["hashtags"].exists(){
            for hashtag in dicTweet["hashtags"].arrayValue{
                arrHashtags = [String]();
                arrHashtags.append(hashtag["text"].string!);
            
            }
        }
        retweetCount = dicTweet["retweet_count"].int!;
        retweeted = dicTweet["retweeted"].bool!;
        user = dicTweet["user"]["name"].string!;
        let pictureURL = NSURL(string: dicTweet["user"]["profile_image_url"].string!)
        profileURL = NSData(contentsOf: pictureURL! as URL)
        if retweeted{
            tweet = tweet + "     retweeted : \(retweetCount)"
        }
        attributedTweet = makeTwitterLook(stringWithTags: tweet as NSString);
    }
    
//    MARK: Tweet Builder
    func makeTwitterLook(stringWithTags:NSString) -> NSMutableAttributedString{
        let regexUser:NSRegularExpression = try! NSRegularExpression(pattern: "@(\\w+)", options: NSRegularExpression.Options.init(rawValue: 0));
        let regexHashTag:NSRegularExpression = try! NSRegularExpression(pattern: "#(\\w+)", options: NSRegularExpression.Options.init(rawValue: 0));
        
        let matchesHashTag: NSArray = regexHashTag.matches(in: stringWithTags as String, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSMakeRange(0, stringWithTags.length)) as NSArray
        
        let matchesUser: NSArray = regexUser.matches(in: stringWithTags as String, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSMakeRange(0, stringWithTags.length)) as NSArray
        
        let attributedMutableString : NSMutableAttributedString = NSMutableAttributedString(string: stringWithTags as String);
        
        let stringLength = stringWithTags.length;
        
        for match in matchesUser as! [NSTextCheckingResult]{
            let wordRange = match.rangeAt(1);
            attributedMutableString.addAttributes([NSFontAttributeName : FontMedium ?? UIFont.systemFont(ofSize: 15)], range: NSMakeRange(0, stringLength)); // Set Font
            let foreColor = UIColor.darkGray;
            attributedMutableString.addAttributes([NSForegroundColorAttributeName : foreColor], range: wordRange); //   Set ForegroundColor
            attributedMutableString.addAttributes(["zUserName" : true], range: wordRange) //    Z custom Key
        }
        
        for match in matchesHashTag as! [NSTextCheckingResult]{
            let wordRange = match.rangeAt(1);
            attributedMutableString.addAttributes([NSFontAttributeName : FontMedium ?? UIFont.systemFont(ofSize: 15)], range: NSMakeRange(0, stringLength)); // Set Font
            let foreColor = UIColor.darkGray;
            attributedMutableString.addAttributes([NSForegroundColorAttributeName : foreColor], range: wordRange); //   Set ForegroundColor
            attributedMutableString.addAttributes(["zHashTag" : true], range: wordRange) //    Z custom Key
        }
        
        let  paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle();
        paragraphStyle.alignment = .justified;
        attributedMutableString.addAttributes([NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, stringLength));
 
        return attributedMutableString;
        
    }

}
